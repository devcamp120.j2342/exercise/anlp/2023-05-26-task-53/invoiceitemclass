import models.InvoiceItem;

public class App {
    public static void main(String[] args) throws Exception {
        InvoiceItem invoiceItem1 = new InvoiceItem("1", "an uong", 8, 5000);
        InvoiceItem invoiceItem2 = new InvoiceItem("2", "pizza", 2, 10000);

        System.out.println(invoiceItem1.toString());
        System.out.println("tổng giá: " + invoiceItem1.getTotal());

        System.out.println(invoiceItem2.toString());
        System.out.println("tổng giá: " + invoiceItem2.getTotal());

    }
}
